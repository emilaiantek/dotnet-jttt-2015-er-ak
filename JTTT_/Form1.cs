﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;

namespace JTTT_
{
    public partial class Form1 : Form
    {
        ZestawZadan Zestaw = new ZestawZadan();

        public Form1()
        {
            InitializeComponent();
        }

        //dodaj
        private void button1_Click(object sender, EventArgs e)
        {
            Zestaw.DodajZadanie(textBox1.Text, textBox2.Text, textBox3.Text, textBox4.Text);
            listBox1.DataSource = Zestaw.Lista_();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        //wykonaj
        private void button2_Click(object sender, EventArgs e)
        {
            Zestaw.Wykonaj();
        }


        //czysc liste
        private void button3_Click(object sender, EventArgs e)
        {
            Zestaw.Wyczysc();
        }

        //deserializacja
        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                Zestaw.Deserializuj();
                listBox1.DataSource = Zestaw.Lista_();
            }
            catch
            {
                MessageBox.Show("Nie udało się zserializować");
            }
        }


        //serializuj
        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                Zestaw.Serializuj();
            }
            catch
            {
                MessageBox.Show("Nie udało się zdeserializować");
            }
        }

    }
}