﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace JTTT_
{
    class ZestawZadan
    {
        BindingList<Zad> Lista = new BindingList<Zad>();
        public BindingList<Zad> Lista_()
        {
            return Lista;
        }

        public void DodajZadanie(string AdresStrony, string Tag, string Adresat, string Nazwa)
        {
            Lista.Add(new Zad(AdresStrony, Tag, Adresat, Nazwa));
        }

        public void Wykonaj()
        {
            foreach (Zad zadanie in Lista)
            {
                zadanie.wykonaj();
            }
        }

        public void Wyczysc()
        {
            Lista.Clear();
        }

        public void Serializuj()
        {
            IFormatter formatter = new BinaryFormatter();

            Stream stream = new FileStream("MyFile.bin", FileMode.Create, FileAccess.Write, FileShare.None);
            formatter.Serialize(stream, Lista);
            stream.Close();
        }

        public void Deserializuj()
        {
            BinaryFormatter formatter = new BinaryFormatter();

            //Reading the file from the server
            FileStream fs = File.Open("MyFile.bin", FileMode.Open);

            object obj = formatter.Deserialize(fs);
            Lista = (BindingList<Zad>)obj;
            

            fs.Flush();
            fs.Close();
            fs.Dispose();
        }
    }
}
