﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;
using System.Web;
using HtmlAgilityPack;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace JTTT_
{
    [Serializable()]
    class Zad
    {
        string AdresObrazka, AdresStrony, Tag, Nazwa;
        const string NazwaObrazka = "obrazek.jpg";
        public string Adresat;
        public override string ToString()
        {
            return "Zadanie: " + Nazwa + " URL: " + AdresStrony + " Tag: " + Tag + " E-mail: " + Adresat;
        }

        public Zad(string _AdresStrony, string _Tag, string _Adresat, string _Nazwa)
        {
            AdresStrony = _AdresStrony;
            Adresat = _Adresat;
            Tag = _Tag;
            Nazwa = _Nazwa;
        }

        public void wykonaj()
        {
            if (ZnajdzObrazek())
            {
                if (PobierzObrazek())
                {
                    if (WyslijMaila())
                    {
                        MessageBox.Show("E-mail został wysłany na adres " + Adresat);
                        Logowanie("Operacja zakończona powodzeniem");
                    }
                    else
                    {
                        MessageBox.Show("Nie udało się wysłać maila  ");
                        Logowanie("Nieudana próba wysłania maila");
                    }
                }
                else
                {
                    MessageBox.Show("Nie udało się pobrać obrazka");
                    Logowanie("Nieudana próba pobrania obrazka");
                }

            }
            else
                Logowanie("Nieudana próba znalezienia tagu na podanej stronie");

        }

        public bool WyslijMaila()
        {
            try
            {
                //tworzenie wiadomosci
                MailMessage Wiadomosc = new MailMessage();
                Wiadomosc.From = new MailAddress("emilaiantek@interia.pl");
                Wiadomosc.To.Add(Adresat);
                Wiadomosc.Subject = "Mail z obrazkiem";
                Wiadomosc.Body = "Przesyłamy Ci maila z obrazkiem związanym z podanym przez Ciebie tagiem: " + Tag + "\n\rPozdrawiamy,\n\rEmilia i Antoni";
                Attachment zalacznik = new Attachment(NazwaObrazka);
                Wiadomosc.Attachments.Add(zalacznik);

                //konfiguracja serwera pocztowego
                SmtpClient smtp = new SmtpClient();
                smtp.Credentials = new NetworkCredential("emilaiantek@interia.pl", "repo123");//adres e-mail i hasło
                smtp.Host = "poczta.interia.pl";//adres smtp
                smtp.Port = 587;//port zależy od dostawcy usługi
                smtp.EnableSsl = true;//włączony system bezpieczeństwa

                //wysylanie wiadomosci

                smtp.Send(Wiadomosc);
            }
            catch
            {
                return false;
            }
            return true;
        }


        public bool PobierzObrazek()
        {
            //pobiera obrazek z internetu i zapisuje go w katalogu głównym programu
            WebClient webClient = new WebClient();
            try
            {
                webClient.DownloadFile(AdresObrazka, NazwaObrazka);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public bool ZnajdzObrazek()
        {
            WebClient Klient = new WebClient();
            //pobieranie źródła strony jako ciągu bajtów
            byte[] Ciag;
            try
            {
                Ciag = Klient.DownloadData(AdresStrony);//niezabezpieczone
            }
            catch
            {
                MessageBox.Show("Nie można odczytać storny");
                return false;
            }

            //odkodowywanie
            string Zrodlo = System.Net.WebUtility.HtmlDecode(Encoding.UTF8.GetString(Ciag));

            HtmlAgilityPack.HtmlDocument ZrodloSformatowane = new HtmlAgilityPack.HtmlDocument();
            ZrodloSformatowane.LoadHtml(Zrodlo);

            //Ograniczenie do nodow "img"
            var nody = ZrodloSformatowane.DocumentNode.Descendants("img");

            // Iterujemy po znalezionych node'ach, az znajdziemy pasujacy obrazek
            foreach (var node in nody)
            {
                if (node.GetAttributeValue("alt", "").Contains(Tag))//jezeli node zawiera Tag
                {
                    AdresObrazka = node.GetAttributeValue("src", "");
                    MessageBox.Show("Znaleziono tag, jego URL to: " + AdresObrazka);
                    return true;
                }
            }
            MessageBox.Show("Nie znaleziono podanego tagu na stronie.");
            return false;
        }

        public void Logowanie(string PrzebiegOperacji)
        {
            string ZawartoscPliku;
            if (File.Exists("JTTT.log"))
            {
                ZawartoscPliku = File.ReadAllText("JTTT.log");
            }
            else
            {
                ZawartoscPliku = "";
            }
            ZawartoscPliku += ("Czas: " + DateTime.Now.ToString() + " Tag: " + Tag + " Adresat: " + Adresat + PrzebiegOperacji + "\r\n");

            File.WriteAllText("JTTT.log", ZawartoscPliku);
        }
    }
}
